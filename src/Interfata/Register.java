package Interfata;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Database.DbConnection;
import Database.EntityManager;
import Database.Entities.User;

public class Register extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton Button_Register;
	private JLabel title;
	private JLabel Username;
	private JLabel Password;
	private JLabel Retype_Password;
	private JTextField text_username;
	private JPasswordField text_password;
	private JPasswordField text_retype_password;
	
	private CategoryFrame categoryFrame;
	
	public Register(){
		
		this.setLayout(null);
		this.setSize(800, 800 / 12 * 9);
		this.setBackground(Color.DARK_GRAY);
		
		Font font2 = new Font("Verdana",Font.BOLD, 26);
		Font font = new Font("Verdana",Font.BOLD, 15);
		
		title = new JLabel("Register");
		title.setBounds(350,20,150,50);
		title.setFont(font2);
		title.setForeground(Color.ORANGE);
		
		
		Username = new JLabel("Username:");
		Username.setBounds(170,140,200,50);
		Username.setFont(font);
		Username.setForeground(Color.ORANGE);
		
		
		text_username = new JTextField(200); 
		text_username.setBounds(350,142,200,50);
		text_username.setFont(font);
		
		
		Password = new JLabel("Password:");
		Password.setBounds(170,242,200,50);
		Password.setFont(font);
		Password.setForeground(Color.ORANGE);
		
		
		text_password = new JPasswordField(200);
		text_password.setBounds(350,242,200,50);
		text_password.setFont(font);
		text_password.setEchoChar('*');
	
		
		Retype_Password = new JLabel("Retype Password:");
		Retype_Password.setBounds(170,342,200,50);
		Retype_Password.setFont(font);
		Retype_Password.setForeground(Color.ORANGE);
		
		
		text_retype_password = new JPasswordField(200);
		text_retype_password.setBounds(350,342,200,50);
		text_retype_password.setFont(font);
		text_retype_password.setEchoChar('*');
		
		Button_Register = new JButton("Register");
		Button_Register.setBounds(570,490,200,50);
		Button_Register.setForeground(Color.DARK_GRAY);
		Button_Register.setBackground(Color.ORANGE);
		Button_Register.addActionListener(new Handler());
		
		this.add(title);
		this.add(Username);
		this.add(text_username);
		this.add(Password);
		this.add(text_password);
		this.add(Retype_Password);
		this.add(text_retype_password);
		this.add(Button_Register);
		
		this.setVisible(false);
		
	}
	
	public void setCategoryFrame (CategoryFrame value){
		this.categoryFrame = value;
	}
	
	private class Handler implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			String textUser = text_username.getText();
			String textPassword = new String(text_password.getPassword());
			String textConfirm = new String(text_retype_password.getPassword());
		
			if (textUser.isEmpty()) {
				JOptionPane.showMessageDialog(null, "User field can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			} else if (textPassword.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Password field can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			} else if (textPassword.equals(textConfirm)) {
					User user = new User();
					user.setUsername(textUser);
					user.setPassword(textPassword);
					
					DbConnection db = DbConnection.getInstance();
					Connection cn = db.getConnection();
					EntityManager em = new EntityManager(cn);
		
					if (em.createUserMapper().save(user)){
						JOptionPane.showMessageDialog(null, "Account has been created!", "Done", JOptionPane.INFORMATION_MESSAGE);
						categoryFrame.setVisible(true);
						setVisible(false);
						
					} else {
						JOptionPane.showMessageDialog(null, "Failed to create account!", "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}else{
					JOptionPane.showMessageDialog(null, "Passwords don\'t match", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
}
