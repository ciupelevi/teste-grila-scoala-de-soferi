package Interfata;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import mainPackage.Main;
import Database.DbConnection;
import Database.EntityManager;
import Database.Entities.Questionnaire;

/**
 * 
 * @author stefan
 *
 */
public class CategoryFrame extends JPanel {

	private static final long serialVersionUID = 1L;

	private JButton buttons[] = new JButton[8];
	private String buttonsName[] = { "Reguli specifice", "Mecanica",
			"Infractiuni si contraventii", "Reguli de prim ajutor",
			"Conducere preventiva", "Conducere ecologica", "Circulatie",
			"Examen" };
	private JLabel title;

	private int x = 50;
	private int y = 70;
	private QuestionnaireView questionnaireViewPannel;
	
	public void setQuestionnaireViewPannel(QuestionnaireView questionnaireViewPannel) {
		this.questionnaireViewPannel = questionnaireViewPannel;
	}

	public CategoryFrame() {

		this.setSize(Main.WIDTH, Main.HEIGHT);
		this.setLayout(null);
		this.setBackground(Color.DARK_GRAY);
		/**
		 * initialize buttons
		 */
		for (int i = 0; i < 8; i++) {
			if (i == 4) {
				x = 550;
				y = 70;
			}

			buttons[i] = createButton(buttonsName[i], x, y, 200, 40);
			buttons[i].setFont(new Font("serif", Font.ITALIC + Font.BOLD, 14));
			buttons[i].setBackground(Color.ORANGE);
			buttons[i].setForeground(Color.DARK_GRAY);
			buttons[i].addActionListener(new Handler());
			y += 150;
		}

		/**
		 * intialize title
		 */
		title = createLabel("Choose category", 300, 0, 300, 50);
		title.setFont(new Font("tahoma", Font.BOLD, 24));
		title.setForeground(Color.ORANGE);

		for (int i = 0; i < 8; i++)
			this.add(buttons[i]);

		add(title);
		this.setVisible(false);
	}

	/**
	 * 
	 * @param name
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return
	 */
	private JButton createButton(String name, int x, int y, int width,
			int height) {
		JButton button = new JButton(name);
		button.setBounds(x, y, width, height);

		return button;
	}

	/**
	 * 
	 * @param name
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return
	 */
	private JLabel createLabel(String name, int x, int y, int width, int height) {
		JLabel label = new JLabel(name);
		label.setBounds(x, y, width, height);

		return label;
	}



	private class Handler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int key = -1;
			DbConnection db = DbConnection.getInstance();
			Connection conn = db.getConnection();
			EntityManager em = new EntityManager(conn);
			Questionnaire questionnaire = null;
			
			if (e.getSource() == buttons[0]) {
			//	System.out.println("Reguli specifice");
				key = 0;
			}
			
			if (e.getSource() == buttons[1]) {
			//	System.out.println("Mecanica");
				key = 1;
			}
			
			if (e.getSource() == buttons[2]) {
			//	System.out.println("Infractiuni si contraventii");
				key = 2;
			}
			
			if (e.getSource() == buttons[3]) {
			//	System.out.println("Reguli de prim ajutor");
				key = 3;
			}
			
			if (e.getSource() == buttons[4]) {
			//	System.out.println("Conducere preventiva");
				key = 4;
			}
			
			if (e.getSource() == buttons[5]) {
			//	System.out.println("Conducere ecologica");
				key = 5;
			}
			
			if (e.getSource() == buttons[6]) {
			//	System.out.println("Circulatie");
				key = 6;
			}
			
			if (e.getSource() == buttons[7]) {
			//	System.out.println("Examen");
				key = 7;
			}
			
			if (key != 7) {
				questionnaire = em.createQuestionnaire(em.createQuestionMapper().findBy("category", ""+key, 0));
			} else {
				//examen
				
			}
			questionnaireViewPannel.setQuestionnaire(questionnaire);
			questionnaireViewPannel.setVisible(true);
			setVisible(false);
		
		}

	}
}
