package Interfata;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Database.DbConnection;
import Database.EntityManager;
import Database.Entities.User;

public class Login extends JPanel {

	private JLabel user;
	private JLabel password;
	private JTextField tuser;
	private JPasswordField tpassword;
	private JButton register;
	private JButton login;
	private Register reg;
	private CategoryFrame categoryView;

	private static final long serialVersionUID = 1L;

	public Login() {

		setSize(800, 800 / 12 * 9);
		this.setLayout(null);
		this.setBackground(Color.DARK_GRAY);

		user = createLabel("USER:", 200, 100, 200, 50);
		password = createLabel("PASSWORD:", 200, 200, 200, 50);

		tuser = createTextField(400, 100, 200, 50);
		tpassword = new JPasswordField();

		register = createButton("Register", 150, 400, 200, 50);
		login = createButton("Login", 500, 400, 200, 50);

		login.addActionListener(new HandlerLogin());
		register.addActionListener(new HandlerLogin());

		tuser.setFont(new Font("Verdana", Font.BOLD, 15));

		tpassword.setBounds(400, 200, 200, 50);
		tpassword.setEchoChar('*');
		tpassword.setFont(new Font("Verdana", Font.BOLD, 15));

		user.setForeground(Color.ORANGE);
		password.setForeground(Color.ORANGE);

		register.setBackground(Color.ORANGE);
		register.setForeground(Color.DARK_GRAY);

		login.setBackground(Color.ORANGE);
		login.setForeground(Color.DARK_GRAY);

		this.add(user);
		this.add(password);
		this.add(tuser);
		this.add(tpassword);
		this.add(register);
		this.add(login);

		this.setVisible(true);
	}


	public void setReg(Register reg) {
		this.reg = reg;
	}
	
	public void setCategory(CategoryFrame cat){
		this.categoryView = cat;
	}

	public JLabel createLabel(String name, int x, int y, int width, int height) {

		JLabel label = new JLabel();
		label.setBounds(x, y, width, height);

		label.setText(name);
		label.setFont(label.getFont().deriveFont(20.f));

		return label;
	}

	public JTextField createTextField(int x, int y, int width, int height) {

		JTextField textField = new JTextField();
		textField.setBounds(x, y, width, height);

		return textField;
	}

	public JButton createButton(String name, int x, int y, int width, int height) {

		JButton reg = new JButton();
		reg.setBounds(x, y, width, height);
		reg.setText(name);

		return reg;
	}

	private class HandlerLogin implements ActionListener {

		public void actionPerformed(ActionEvent event) {

			if (event.getSource() == login) {
				String username = tuser.getText();
				String passwordname = new String(tpassword.getPassword());

				if (username.isEmpty()) {
					JOptionPane.showMessageDialog(null, "EmptyUser", "User", JOptionPane.ERROR_MESSAGE);
				} else if (passwordname.isEmpty()) {
					JOptionPane.showConfirmDialog(null, "EmptyPassword", "Password", JOptionPane.ERROR_MESSAGE);
				} else {

					DbConnection db = DbConnection.getInstance();
					Connection conn = db.getConnection();

					EntityManager em = new EntityManager(conn);

					User user = em.createUserMapper().findOneBy("username", username);

					if (!(user instanceof User)) {
						JOptionPane.showMessageDialog(null, "User don/'t exist", "User", JOptionPane.ERROR_MESSAGE);
					} else if (user.passwordMatch(passwordname)) {
						setVisible(false);
						categoryView.setVisible(true);
						
					}
				}

			} else {

				setVisible(false);
				reg.setVisible(true);
			}
		}
	}

}
