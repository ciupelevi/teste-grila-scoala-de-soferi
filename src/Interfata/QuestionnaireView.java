package Interfata;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import mainPackage.Main;
import Database.Entities.Questionnaire;

public class QuestionnaireView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea question;
	private JLabel variante;
	private JLabel A, B, C;
	private JTextArea variantaA, variantaB, variantaC;
	private JButton VA, VB, VC;
	private JButton late, send, delete;
	private JLabel lQuestion, categorie, tema, jtema, raspunsCorect, scorCorect, raspunsGresit, scorGresit, raspunsRamas, scorRamas, timpRamas, timer;
	protected Questionnaire questionnaire = null;

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}
	
	public QuestionnaireView() {

		this.setBackground(Color.LIGHT_GRAY);
		setSize(Main.WIDTH, Main.HEIGHT);
		this.setLayout(null);
		
		question = createTextArea(10, 70, 385, 240);

		variante = createLabel("Variante de raspuns:", 10, 275, 300, 100);
		A = createLabel("A:", 10, 302, 100, 100);
		B = createLabel("B:", 10, 332, 100, 100);
		C = createLabel("C:", 10, 362, 100, 100);

		variantaA = createTextArea(30, 340, 750, 25);
		variantaB = createTextArea(30, 370, 750, 25);
		variantaC = createTextArea(30, 400, 750, 25);

		VA = createButton("", 90, 440, 50, 50);
		VB = createButton("", 370, 440, 50, 50);
		VC = createButton("", 640, 440, 50, 50);
		
		/**
		 * set button Images
		 */
		
		VA.setIcon(new ImageIcon(getClass().getResource("A.png")));
		VB.setIcon(new ImageIcon(getClass().getResource("B.png")));
		VC.setIcon(new ImageIcon(getClass().getResource("C.png")));
		
		
		late = createButtonNormal("Raspunde mai tarziu", 25, 505, 200, 50);
		delete = createButtonNormal("Sterge Raspuns", 300, 505, 200, 50);
		send = createButtonNormal("Trimite raspuns", 575, 505, 200, 50);	
		
		/**
		 * set button Images
		 */
		late.setIcon(new ImageIcon(getClass().getResource("tarziu.png")));
		delete.setIcon(new ImageIcon(getClass().getResource("sterge.png")));
		send.setIcon(new ImageIcon(getClass().getResource("trimite.png")));
		

		lQuestion = createLabel("Intrebare #NR", 10, 30, 100, 50);

		categorie = createLabel("Cat: B", 10, 10, 100, 50);
		
		tema = createLabel("Tema:", 60, 10, 100, 50);
		jtema = createLabel("infractiuni, contraventii ", 110, 10, 200, 50);
		jtema.setForeground(Color.YELLOW);//new Color(255, 129, 0));
		
		raspunsCorect = createLabel("Raspunsuri corecte:", 310, 10, 200, 50);
		scorCorect = createLabel("10", 460, 10, 200, 50);
		scorCorect.setForeground(new Color(10, 180, 0));
		
		raspunsGresit = createLabel("Gresite:", 485, 10, 200, 50);
		scorGresit = createLabel("0", 545, 10, 200, 50);
		scorGresit.setForeground(Color.RED);
		
		raspunsRamas = createLabel("Ramase:", 565, 10, 200, 50);
		scorRamas = createLabel("26", 630, 10, 200, 50);
		scorRamas.setForeground(Color.BLUE);
		
		timpRamas = createLabel("Timp ramas:", 660, 10, 200, 50);
		timer = createLabel("30:00",750, 10, 200, 50);
		timer.setForeground(Color.RED);
		
		this.add(timer);
		this.add(scorRamas);
		this.add(scorGresit);
		this.add(scorCorect);
		this.add(jtema);
		
		this.add(question);
		this.add(variante);

		this.add(A);
		this.add(B);
		this.add(C);

		this.add(variantaA);
		this.add(variantaB);
		this.add(variantaC);

		this.add(VA);
		this.add(VB);
		this.add(VC);

		this.add(lQuestion);
		this.add(categorie);
		this.add(tema);
		this.add(raspunsCorect);
		this.add(raspunsGresit);
		this.add(raspunsRamas);
		this.add(timpRamas);

		this.add(late);
		this.add(delete);
		this.add(send);
		
		this.setVisible(false);

	}

	public JButton createButton(String name, int x, int y, int width, int height) {

		JButton reg = new JButton();
		reg.setBounds(x, y, width, height);
		reg.setText(name);

		reg.setFont(reg.getFont().deriveFont(25.f));

		return reg;
	}

	public JButton createButtonNormal(String name, int x, int y, int width, int height) {

		JButton reg = new JButton();
		reg.setBounds(x, y, width, height);
		reg.setText(name);

		reg.setFont(reg.getFont().deriveFont(12.f));

		return reg;
	}

	public JTextArea createTextArea(int x, int y, int width, int height) {

		JTextArea textField = new JTextArea();
		textField.setBounds(x, y, width, height);

		return textField;
	}

	public JLabel createLabel(String name, int x, int y, int width, int height) {

		JLabel label = new JLabel();
		label.setBounds(x, y, width, height);

		label.setText(name);
		label.setFont(label.getFont().deriveFont(15.f));

		return label;
	}

	public void paintComponent(Graphics g){
	
		super.paintComponent( g);
		g.setColor(Color.YELLOW);
		g.drawRect(400, 70, 385, 240);
	
	}
	
	
}
