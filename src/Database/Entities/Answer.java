package Database.Entities;

public class Answer {

	protected int id;
	
	protected String answer1;

	protected String answer2;
	
	protected String answer3;
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstAnswer() {
		return answer1;
	}

	public void setFirstAnswer(String answer) {
		this.answer1 = answer;
	}

	public String getSecondAnswer() {
		return answer2;
	}

	public void setSecondAnswer(String answer) {
		this.answer2 = answer;
	}
	
	public String getThirdAnswer() {
		return answer3;
	}

	public void setThirdAnswer(String answer) {
		this.answer3 = answer;
	}
}
