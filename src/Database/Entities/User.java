package Database.Entities;

public class User {

	protected int id;
	
	protected String username;
	
	protected String password;
	
	public User() {
		
	}
	
	public User(String user, String pass) {
		this.username = user;
		this.password = pass;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Check if a password matches the user's password.
	 * 
	 * @param password
	 * @return boolean
	 */
	public boolean passwordMatch(String password) {
		return this.password.equals(password);
	}
}
