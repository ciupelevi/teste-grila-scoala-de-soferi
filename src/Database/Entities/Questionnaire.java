package Database.Entities;

public class Questionnaire {

	protected Question[] questions;
	
	protected int right;
	
	protected int wrong;
	
	protected int total;
	
	public Questionnaire(Question[] questions) {
		this.questions = questions;
		this.total = questions.length;
		
	}

	public Question[] getQuestions() {
		return questions;
	}

	public void setQuestions(Question[] questions) {
		this.questions = questions;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}

	public int getWrong() {
		return wrong;
	}

	public void setWrong(int wrong) {
		this.wrong = wrong;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
