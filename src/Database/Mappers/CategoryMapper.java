package Database.Mappers;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import Database.Entities.Category;

public class CategoryMapper {

	private Connection conn = null;
	
	public CategoryMapper(Connection conn) {
		this.conn = conn;
	}
	public Category findOneBy(String key, String value) {
		PreparedStatement pst;
		ResultSet result;

		Category category = null;
		try {
			pst = this.conn.prepareStatement("SELECT * FROM categories WHERE " + key + "=? LIMIT 1");
			pst.setString(1, value);
			category = new Category();
			result = pst.executeQuery();
			
			while (result.next()) {
				category.setId(result.getInt("id"));
				category.setName(result.getString("name"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return category;
	}

	public Category[] findBy(String key, String value) {
		PreparedStatement pst;
		ResultSet result;

		List<Category> categories = new ArrayList<Category>();
		
		try {
			pst = this.conn.prepareStatement("SELECT * FROM categories WHERE " + key + "=?");
			pst.setString(1, value);
			result = pst.executeQuery();
			
			while (result.next()) {
				Category category = new Category();
				category.setId(result.getInt("id"));
				category.setName(result.getString("name"));
				
				categories.add(category);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (Category[])categories.toArray(new Category[categories.size()]);
	}
	
	public Category[] all() {
		PreparedStatement pst;
		ResultSet result;

		List<Category> categories = new ArrayList<Category>();
		
		try {
			pst = this.conn.prepareStatement("SELECT * FROM categories");
			result = pst.executeQuery();
			
			while (result.next()) {
				Category category = new Category();
				category.setId(result.getInt("id"));
				category.setName(result.getString("name"));
				
				categories.add(category);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (Category[])categories.toArray(new Category[categories.size()]);
	}
}
