package Database.Mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Database.Entities.User;

public class UserMapper {

	private Connection conn = null;

	public UserMapper(Connection conn) {
		this.conn = conn;
	}
	
	public User findOneBy(String key, String value) {
		PreparedStatement pst;
		ResultSet result;

		User user = null;
		try {
			pst = this.conn.prepareStatement("SELECT * FROM users WHERE " + key + "=? LIMIT 1");
			pst.setString(1, value);
			result = pst.executeQuery();
			
			while (result.next()) {
				user = new User();
				user.setId(result.getInt("id"));
				user.setUsername(result.getString("username"));
				user.setPassword(result.getString("password"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	public User[] findBy(String key, String value) {
		PreparedStatement pst;
		ResultSet result;

		List<User> users = new ArrayList<User>();
		
		try {
			pst = this.conn.prepareStatement("SELECT * FROM users WHERE " + key + "=?");
			pst.setString(1, value);
			result = pst.executeQuery();
			
			while (result.next()) {
				User user = new User();
				user.setId(result.getInt("id"));
				user.setUsername(result.getString("username"));
				user.setPassword(result.getString("password"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (User[])users.toArray(new User[users.size()]);
	}
	
	public User[] all() {
		PreparedStatement pst;
		ResultSet result;

		List<User> users = new ArrayList<User>();
		
		try {
			pst = this.conn.prepareStatement("SELECT * FROM users");
			
			result = pst.executeQuery();
			
			while (result.next()) {
				User user = new User();
				user.setId(result.getInt("id"));
				user.setUsername(result.getString("username"));
				user.setPassword(result.getString("password"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (User[])users.toArray(new User[users.size()]);
	}	
	
	public boolean save(User user)
	{
		boolean saved = false;
		PreparedStatement pst;
		
		try {
			pst = this.conn.prepareStatement("INSERT INTO users (username, password) VALUES (?, ?)");
			pst.setString(1, user.getUsername());
			pst.setString(2, user.getPassword());
			
			pst.executeUpdate();
			saved = true;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		
		return saved;
	}
	
}
