package Database.Mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Database.Entities.Answer;

public class AnswerMapper {

	private Connection conn = null;
	
	public AnswerMapper(Connection conn) {
		this.conn = conn;
	}
	
	public Answer findOneById(int id) {
		PreparedStatement pst;
		ResultSet result;

		Answer answer = null;
		try {
			pst = this.conn.prepareStatement("SELECT * FROM questionanswers WHERE id=? LIMIT 1");
			pst.setInt(1, id);
			answer = new Answer();
			result = pst.executeQuery();
			
			while (result.next()) {
				answer.setId(result.getInt("id"));
				answer.setFirstAnswer(result.getString("answer1"));
				answer.setSecondAnswer(result.getString("answer2"));
				answer.setThirdAnswer(result.getString("answer3"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return answer;
	}
}
