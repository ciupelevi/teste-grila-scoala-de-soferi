package Database.Mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Database.Entities.Answer;
import Database.Entities.Category;
import Database.Entities.Question;

public class QuestionMapper {

	private Connection conn = null;
	
	public QuestionMapper(Connection conn) {
		this.conn = conn;
	}
	
	public Question findOneBy(String key, String value) {
		PreparedStatement pst;
		ResultSet result;
		AnswerMapper answerMapper = new AnswerMapper(this.conn);
		CategoryMapper categoryMapper = new CategoryMapper(this.conn);
		
		Question question = null;
		try {
			pst = this.conn.prepareStatement("SELECT * FROM questions WHERE " + key + "=? LIMIT 1");
			pst.setString(1, value);
			result = pst.executeQuery();
			
			while (result.next()) {
				question = new Question();
				Answer answer = answerMapper.findOneById(result.getInt("answers"));
				Category category = categoryMapper.findOneBy("id", result.getString("category"));
				question.setId(result.getInt("id"));
				question.setQuestion(result.getString("question"));
				question.setImagePath(result.getString("imagePath"));
				question.setRightAnswer(result.getInt("rightAnswer"));
				question.setAnswers(answer);
				question.setCategory(category);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return question;
	}
	
	public Question[] findBy(String key, String value, int limit) {
		PreparedStatement pst;
		ResultSet result;
		AnswerMapper answerMapper = new AnswerMapper(this.conn);
		CategoryMapper categoryMapper = new CategoryMapper(this.conn);
		
		List<Question> questions = new ArrayList<Question>();
		Question[] returnQuestions = null;
		try {
			
			pst = this.conn.prepareStatement("SELECT * FROM questions WHERE " + key + "=?");
			
			pst.setString(1, value);
			result = pst.executeQuery();
			while (result.next()) {
				Answer answer = answerMapper.findOneById(result.getInt("answers"));
				Category category = categoryMapper.findOneBy("id", result.getString("category"));
				Question question = new Question();
				question.setId(result.getInt("id"));
				question.setQuestion(result.getString("question"));
				question.setImagePath(result.getString("imagePath"));
				question.setRightAnswer(result.getInt("rightAnswer"));
				question.setAnswers(answer);
				question.setCategory(category);
				
				questions.add(question);
			}
			
			returnQuestions = (Question[])questions.toArray(new Question[questions.size()]);
			
			if (limit != 0) {
				Random rand = new Random();
				Question[] selectedQuestions = new Question[limit];
				for (int i = 0; i < limit; i++) {
					int j = rand.nextInt(returnQuestions.length) ;
					selectedQuestions[i] = returnQuestions[j];
				}
				
				returnQuestions = selectedQuestions;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return returnQuestions;
	}
	
	public Question[] all() {
		PreparedStatement pst;
		ResultSet result;
		AnswerMapper answerMapper = new AnswerMapper(this.conn);
		CategoryMapper categoryMapper = new CategoryMapper(this.conn);
		
		List<Question> questions = new ArrayList<Question>();
		Question[] returnQuestions = null;
		try {
			pst = this.conn.prepareStatement("SELECT * FROM questions");
			
			result = pst.executeQuery();
			while (result.next()) {
				Answer answer = answerMapper.findOneById(result.getInt("answers"));
				Category category = categoryMapper.findOneBy("id", result.getString("category"));
				Question question = new Question();
				question.setId(result.getInt("id"));
				question.setQuestion(result.getString("question"));
				question.setImagePath(result.getString("imagePath"));
				question.setRightAnswer(result.getInt("rightAnswer"));
				question.setAnswers(answer);
				question.setCategory(category);
				
				questions.add(question);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		returnQuestions = (Question[])questions.toArray(new Question[questions.size()]);
		
		return returnQuestions;
	}
}
