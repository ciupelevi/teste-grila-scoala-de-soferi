package Database;

import Database.Entities.*;
import Database.Mappers.*;
import java.sql.*;

public class EntityManager {
	
	private Connection conn = null;
	
	public EntityManager(Connection conn) {
		this.conn = conn;
	}
	
	public User createUserEntity() {
		return new User();
	}
	
	public UserMapper createUserMapper() {
		return new UserMapper(this.conn);
	}
	
	public Category createCategory() {
		return new Category();
	}
	
	public CategoryMapper createCategoryMapper() {
		return new CategoryMapper(this.conn);
	}
	
	public Question createQuestion() {
		return new Question();
	}
	
	public QuestionMapper createQuestionMapper() {
		return new QuestionMapper(this.conn);
	}
	
	public Answer createAnswer() {
		return new Answer();
	}
	
	public AnswerMapper createAnswerMapper() {
		return new AnswerMapper(this.conn);
	}
	
	public Questionnaire createQuestionnaire(Question[] questions) {
		return new Questionnaire(questions);
	}
	
}
