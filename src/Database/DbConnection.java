package Database;

import java.sql.*;

public class DbConnection {

	private static DbConnection instance = null;
	private Connection conn;
	private boolean isConnected = false;
	
	private String host = "localhost";
	private String database = "scoala_soferi";
	private String user = "root";
	private String password = "";
	
	/**
	 * 
	 * @return DbConnection
	 */
	public static DbConnection getInstance() {
		if (DbConnection.instance == null)
		{
			DbConnection.instance = new DbConnection();
		}
		return DbConnection.instance;
	}
	
	private DbConnection () {
		connect(host, database, user, password);
	}
	
	/**
	 * 
	 * @param host
	 * @param database
	 * @param user
	 * @param password
	 */
	private void connect(String host, String database, String user, String password) {
		String hostString = "jdbc:mysql://" + host + "/"+ database;
		
		try{
			this.conn  = (Connection)DriverManager.getConnection(hostString, user, password);
			this.isConnected = true;
		} catch (SQLException e) {
			this.isConnected = false;
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return Connection
	 */
	public Connection getConnection() {
		if (!isConnected) {
			connect(host, database, user, password);
		}
		
		return this.conn;
	}
	
	public void closeConnection() {
		try {
			this.conn.close();
			this.isConnected = false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @return boolean
	 */
	public boolean isConnected() {
		return this.isConnected;
	}
}
