package mainPackage;
import javax.swing.JFrame;

import Interfata.CategoryFrame;
import Interfata.Login;
import Interfata.QuestionnaireView;
import Interfata.Register;

/**
 * 
 * @author stefan
 *
 */
public class MainFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private Register registerPanel;
	private CategoryFrame categoryFramePanel;
	private Login loginPanel;
	private QuestionnaireView questionnaireView;
	
	public MainFrame(){
		
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(Main.WIDTH, Main.HEIGHT);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		
		registerPanel = new Register();
		categoryFramePanel = new CategoryFrame();
		loginPanel = new Login();
		questionnaireView = new QuestionnaireView();
		registerPanel.setCategoryFrame(categoryFramePanel);
		categoryFramePanel.setQuestionnaireViewPannel(questionnaireView);
		loginPanel.setCategory(categoryFramePanel);
		loginPanel.setReg(registerPanel);
		
		add(categoryFramePanel);
		add(registerPanel);
		add(loginPanel);
		add(questionnaireView);
		
		this.setVisible(true);
	}
}
