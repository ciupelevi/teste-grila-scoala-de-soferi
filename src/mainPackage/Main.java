package mainPackage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Interfata.Login;


public class Main {

	public static final int WIDTH = 800 ;
	public static final int HEIGHT = WIDTH / 12 * 9;
	
	/**
	 * Show frame with JPanel.
	 * 
	 * @param frame
	 * @param panel
	 */
	public static void Show(JFrame frame,JPanel panel ){
		if (panel instanceof JPanel)
			frame.add(panel);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	/**
	 * Start point.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		MainFrame frame = new MainFrame();
		frame.setVisible(true);
		
	}

}
